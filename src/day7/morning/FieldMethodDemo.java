package day7.morning;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodDemo {

	public static void main(String[] args) throws Exception{
		PoliceStation p1 = new PoliceStation();
		Naxalite ramu = new Naxalite();
		Politician pol = new Politician();
		p1.arrest(pol);
	}

}
class PoliceStation{
	public void arrest(Object ob) throws Exception{
		Class c = ob.getClass();
		Field field = c.getField("name");
		System.out.println(field.get(ob));
		Field fields[] = c.getFields();
		for(Field f:fields){
			System.out.println(f.getName());
		}
		Method met = c.getMethod("work",new Class[] {String.class});
		met.invoke(ob,new Object[] {"Guns"});	
		tortureRoom(ob);
	}
	public void tortureRoom(Object o) throws Exception{
		Class c = o.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(o));
		
		Method met = c.getDeclaredMethod("secretWork", new Class[]{String.class});
		met.setAccessible(true);
		met.invoke(o, new Object[]{"in bombay.."});
		
	}
}
class Naxalite{
	public String name = "I am naxalite";
	public void work(String s){
		System.out.println("Killing innocent with .."+s);
	}
}
class Politician{
	public String name = "Good...Holy man...";
	private String secretName = "Gangster..";
	public void work(String s){
		System.out.println("Social work....."+ s);
	}
	private void secretWork(String s){
		System.out.println("looting...murdering..."+s);
	}
}