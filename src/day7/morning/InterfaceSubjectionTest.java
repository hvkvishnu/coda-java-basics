package day7.morning;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class InterfaceSubjectionTest {
	private static Object object;
	
	@BeforeClass
	public static void initialize(){
		object = new InterfaceSubjectionDemo().createObj(new Class[]{Doctor.class,Pilot.class}, new Object[]{new AlopathyMedicalCollege(),new JetAcademy()});
	}
	
	@Test
	public void checkDoctorObj(){ 
		assertFalse("Check doctor object",((Doctor)object).getClass() == Doctor.class);
//		System.out.println(((Doctor)object).getClass() +":"+Doctor.class);
	}
	
	@Test
	public void checkPilotObj(){ 
		assertFalse("Check doctor object",((Pilot)object).getClass() == Pilot.class);
	}
	
	@Test
	public void testDoctor(){
		Doctor doc = (Doctor)object;
		assertEquals("alopathy cure for corona started...", doc.doCure());
	}
	
	@Test
	public void testPilot(){
		Pilot ramu = (Pilot)object;
		assertEquals("learn to fly.....",ramu.fly());
	}
	
	@Test
	public void checkSame(){
		assertSame((Doctor)object, (Pilot)object);
	}
}
