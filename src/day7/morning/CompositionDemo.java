package day7.morning;

public class CompositionDemo {

	public static void main(String[] args) {
		IceCream iceCream = new Vanilla(new Nuts(new Fruits()));
		System.out.println("Vanilla + nuts + fruits :"+iceCream.cost());
	}

}
abstract class IceCream{
	public abstract int cost();
}
abstract class Cream extends IceCream{
	
}
class Vanilla extends Cream{
	IceCream cream;
	public Vanilla() {
		
	}
	public Vanilla(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		// TODO Auto-generated method stub
		if(cream!=null) {
			return 10+cream.cost();
		}
		else {
			return 10;
		}
	}
}
class Strawberry extends Cream{
	IceCream cream;
	public Strawberry() {
		// TODO Auto-generated constructor stub
	}
	public Strawberry(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null) {
			return 15+cream.cost();
		}
		else {
			return 15;
		}
	}
}
class Choclate extends Cream{
	IceCream cream;
	public Choclate() {
		// TODO Auto-generated constructor stub
	}
	public Choclate(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null) {
			return 20+cream.cost();
		}
		else {
			return 20;
		}
	}
}
abstract class Ingredients extends IceCream{
	
}
class Nuts extends Ingredients{
	IceCream cream;
	public Nuts() {
		// TODO Auto-generated constructor stub
	}
	public Nuts(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null) {
			return 5+cream.cost();
		}
		else {
			return 5;
		}
	}
}
class Fruits extends Ingredients{
	IceCream cream;
	public Fruits() {
		// TODO Auto-generated constructor stub
	}
	public Fruits(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null) {
			return 10+cream.cost();
		}
		else {
			return 10;
		}
	}
}