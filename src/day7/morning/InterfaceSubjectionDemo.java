package day7.morning;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.print.Doc;

public class InterfaceSubjectionDemo {
	
	public Object createObj(Class[] c,Object[] obj){
		Object object = Proxy.newProxyInstance(Humani.class.getClassLoader(), c, new MyInvocationHandler(obj));
		return object;
	}

	public static void main(String[] args) {
		AlopathyMedicalCollege stanley = new AlopathyMedicalCollege();
		AyurvedaMedicalCollege ayush = new AyurvedaMedicalCollege();
		JetAcademy jet = new JetAcademy();
		
		InterfaceSubjectionDemo isd = new InterfaceSubjectionDemo();
		Object object = isd.createObj(new Class[] {Doctor.class,Pilot.class}, new Object[] {jet,stanley});
		Doctor doctorVishnu = (Doctor)object;
		Pilot pilotVishnu = (Pilot)object;
		System.out.println(doctorVishnu.doCure());
		//pilotVishnu.fly();
		System.out.println(pilotVishnu.fly());
		
		System.out.println((Doctor)object+":"+(Pilot)object+":"+object);
	}
}
class MyInvocationHandler implements InvocationHandler{
	Object obj[];
	Object ob;
	
	public MyInvocationHandler(Object obj[]) {
		this.obj = obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("invoke method.....");
//		method.invoke(obj[0], args);
//		return null;
		for(int i=0; i<obj.length; i++){
			try{
				ob = method.invoke(obj[i], args);
			}catch(Exception e){}
		}
		return ob;
	}
}

interface Doctor{
	public String doCure();
	public void doGiveMedicine();
}
interface Nurse{
	public void nursing();
}
interface Pilot{
	public String fly();
}
interface Steward{
	public void serve(); 
}
class AlopathyMedicalCollege implements Doctor,Nurse{
	@Override
	public String doCure() {
		return ("alopathy cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("No medicine.....");
	}
	@Override
	public void nursing() {
		System.out.println("nursing....");
	}
}
class AyurvedaMedicalCollege implements Doctor{
	@Override
	public String doCure() {
		return("ayurveda cure for corona started..");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("Have kabasara kudineer to cure corona...");
	}
}
class JetAcademy implements Pilot,Steward{
	@Override
	public String fly() {
		return("learn to fly.....");
	}
	@Override
	public void serve() {
		System.out.println("serve....");
	}
}
class Humani{
	
}