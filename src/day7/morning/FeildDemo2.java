package day7.morning;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FeildDemo2 {

	public static void main(String[] args) throws Exception{
		Alien jadu = new Alien();
		Child baby = new Child();
		Student ramu = new Student();
		jadu.interrogate(ramu);
	}

}
class Alien{
	public void interrogate(Object ob) throws Exception{
		Class c = ob.getClass();
		Method met = c.getMethod("sayHello");
		Constructor cons = c.getConstructor();
		System.out.println("Constructor..."+cons);
		met.invoke(ob);
		
		hypnotise(ob);
	}
	public void hypnotise(Object obj) throws Exception{
		Class c = obj.getClass();
		//Field field = c.getDeclaredField("favToy");
		Field field = c.getDeclaredField("phone");
		field.setAccessible(true);
		System.out.println(field.get(obj));
	}
}
abstract class Human{
	abstract public void sayHello();
}
class Child extends Human{
	private String favToy = "cowboy";
	public Child() {
		System.out.println("bla..bla..child");
	}
	@Override
	public void sayHello() {
		System.out.println("I am child....");
	}
}
class Student extends Human{
	private int phone = 225247;
	public Student() {
		System.out.println("Student here...");
	}
	@Override
	public void sayHello() {
		System.out.println("I am student...");
	}
}

