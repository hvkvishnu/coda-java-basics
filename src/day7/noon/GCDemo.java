package day7.noon;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class GCDemo {

	public static void main(String[] args) {
		Runtime r= Runtime.getRuntime();
		System.out.println("Before thathas birth..."+r.freeMemory());
		GrandFather thatha = new GrandFather();
		System.out.println("After thathas birth..."+r.freeMemory());
		//thatha go to kasi
		//SoftReference<GrandFather> soft = new SoftReference<GrandFather>(thatha);
		WeakReference<GrandFather> weak = new WeakReference<GrandFather>(thatha);
		thatha = null;
		System.out.println("After thathas death..."+r.freeMemory());
		System.out.println("We will do kariyam...");
		r.gc();//in reality you wont call gc, jvm calls gc when it is needed
		System.out.println("After kariyam...."+r.freeMemory());
		//return from kasi..
		thatha = weak.get();
		System.out.println(thatha.age);
	}
}
class GrandFather{
	String age;
	private String gold = "Under the tree..";
	public GrandFather() {
		for(int i=0; i<10000;i++){
			age = new String(i+" ");
		}
	}
	private String getGold(){
		return "The gold is "+gold;
	}
	@Override
	protected void finalize() throws Throwable {
		System.out.println("finalized called..."+getGold());
	}
}

