package day7.noon;

public class PrototypeDemo {
	public static void main(String[] args) {
		//first scenario
		System.out.println("first scenario - two object created..");
		Sheep sheep1 = new Sheep();
		Sheep sheep2 = new Sheep();
		sheep1.name = "mother";
		sheep2.name = "duplicate";
		System.out.println(sheep1.name+":"+sheep2.name);
		//second scenario
		System.out.println("second scenario - one object created with two refernce");
		Sheep s1 =new Sheep();
		Sheep s2 = s1; 
		s1.name = "mother";
		s2.name = "dup..dup";
		System.out.println(s1.name+":"+s2.name);
		//third scenario
		System.out.println("Third scenario - clone / proto - Resources are shared but properties are unique - "
				+ "one object with two references but data is unique");
		Sheep mothersheep=new Sheep();
		Sheep dolly=mothersheep.createProto();
		mothersheep.name="i am the mother sheep...";
		dolly.name="i am dolly the clone...";
		System.out.println(mothersheep.name+":"+dolly.name);
		
		
	}
}
class Sheep implements Cloneable{
	String name;
	public Sheep() {
		System.out.println("Sheep constructo called...");
	}
	public Sheep createProto(){
		try{
			return (Sheep)super.clone();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
