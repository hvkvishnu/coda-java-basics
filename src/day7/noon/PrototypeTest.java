package day7.noon;

import org.junit.Test;
import static org.junit.Assert.*;
public class PrototypeTest {
	@Test
	public void testFirstScenario(){
		Sheep sheep1 = new Sheep();
		Sheep sheep2 = new Sheep();
		
		assertNotSame("Two differnt object",sheep1, sheep2);
	}
	
	@Test
	public void testSecondScenario(){
		Sheep s1 = new Sheep();
		Sheep s2 = s1;
		assertSame("One object assigning",s2, s1);
	}
	
	@Test
	public void testThirdScenario(){
		Sheep mother = new Sheep();
		Sheep dolly = mother.createProto();
		assertNotSame(dolly, mother);
	}
}
