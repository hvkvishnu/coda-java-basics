package day2.exercise;

public class ObjTest {

	public static void main(String[] args) {
		ClassC cc = new ClassC();
	}
}
class ClassA{
	public ClassA(int value) {
		System.out.println("This is class A....with "+value);
	}
}
class ClassB{
	public ClassB(int value) {
		System.out.println("This is class B...with "+value);
	}
}
class ClassC extends ClassA{
	public ClassC() {
		super(10);
		ClassB cb = new ClassB(5);
	}
}
