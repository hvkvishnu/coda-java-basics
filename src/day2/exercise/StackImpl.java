package day2.exercise;

public class StackImpl {

	public static void main(String[] args) {
		Stack stk = new Stack(5);
		stk.push(1);
		stk.push(2);
		stk.push(3);
		stk.push(4);
		stk.push(5);
		stk.push(6);
		
		System.out.println(stk.pop());
		System.out.println(stk.pop());
		System.out.println(stk.pop());
		System.out.println(stk.pop());
		System.out.println(stk.pop());
		System.out.println(stk.pop());
	}

}

class Stack{
	private int size;
	private int position;
	private int stack[];
	public Stack(int size) {
		this.size = size;
		this.position = -1;
		this.stack = new int[size];
	}
	
	private boolean isOverFlow(){
		return this.position > (this.size-2);
	}
	
	public void push(int n){
		if(!this.isOverFlow()){
			this.stack[++this.position] = n;
			System.out.println("Item pushed is "+ n);
		}
		else{
			System.out.println("Stack full");
		}
	}
	
	private boolean isNull(){
		return this.position == -1;
	}
	public int pop(){
		if(this.isNull()){
			System.out.println("Stack empty");
			return -1;
		}
		else{
			return this.stack[this.position--];
		}
	}
	
}