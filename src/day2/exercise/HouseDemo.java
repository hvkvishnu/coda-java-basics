package day2.exercise;

public class HouseDemo {

	public static void main(String[] args) {
		House house = new House();
		double total = house.totalCost(105.45, 85.67);
		System.out.println("Total cost for constructing house... "+total);
	}

}
class Window{
	private final int cost = 550;
	public double calculateWindowCost(double area){
		return cost * area;
	}
}
class Door{
	private final int cost = 700;
	public double calculateDoorCost(double area){
		return cost * area;
	}
}
class House{
	Window window = new Window();
	Door door = new Door();
	double doorCost,windowCost;
	public double totalCost(double doorArea,double windowArea){
		doorCost = door.calculateDoorCost(doorArea);
		windowCost = window.calculateWindowCost(windowArea);
		return doorCost + windowCost;
	}
}