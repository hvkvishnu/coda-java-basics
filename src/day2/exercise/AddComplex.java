package day2.exercise;

public class AddComplex {

	public static void main(String[] args) {
		Complex c1 = new Complex(5, 4);
		System.out.println("First complex number....");
		c1.printComplex();
		Complex c2 = new Complex(11, 5);
		System.out.println("Second complex number....");
		c2.printComplex();
		Complex res = c2.add(c1);
		System.out.println("After adding....");
		res.printComplex();
	}

}

class Complex{
	int first,last;
	public Complex() {
		
	}
	public Complex(int x, int y) {
		this.first = x;
		this.last = y;
	}
	
	public Complex add(Complex c){
		Complex result = new Complex();
		result.first =this.first + c.first;
		result.last = this.last + c.last;
		return result;
	}
	public void printComplex(){
		System.out.println(this.first +" + "+this.last+"i");
	}
	
}
