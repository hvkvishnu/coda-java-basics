package day2.exercise;

public class AccessSpecDemo {

	public static void main(String[] args) {
		B b = new B();
		b.getThingsFromA();
	}
}

class A{
	private int pri = 10;
	protected int pro = 20;
	public int pub = 30;
	
	private void privateMethod(){
		System.out.println("This is a private method...");
	}
	protected void protectedMethod(){
		System.out.println("This is a protected method....");
	}
	public void publicMethod(){
		System.out.println("This is a public method...");
	}
}
class B extends A{
	public void getThingsFromA(){
		System.out.println("Calling members and methods from A...");
		//privateMethod();
		//System.out.println(pri);
		protectedMethod();
		publicMethod();
		System.out.println("public variable:"+pub+" protected variable:"+pro);
		
	}
}
