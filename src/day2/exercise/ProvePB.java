package day2.exercise;

public class ProvePB {
	public void printArray(int a[]){
		for(int i : a){
			System.out.print(i+" ");
		}
		System.out.println("");
	}
	public void change(int arr[]){
		arr[0]=7;
		arr[1]=8;
	}
	public static void main(String[] args) {
		int value = 5;
		int arr[] = {1,2,3,4,5};
		
		System.out.println("Value before passing : "+value);
		PBV pbv = new PBV(value);
		System.out.println("After changing..."+value);
		
		PBR pbr = new PBR();
		System.out.println("Before changing object's value..."+pbr.value);
		pbr.change(25);
		System.out.println("After changing object's value..."+pbr.value);
		
		System.out.println("Before passing array...");
		ProvePB ppb = new ProvePB();
		ppb.printArray(arr);
		System.out.println("After passing array...");
		ppb.change(arr);
		ppb.printArray(arr);
		
		
		
	}

}

class PBV{
	public PBV(int value) {
		value = 10;
		System.out.println("Inside PBV : "+value);
	}
}

class PBR{
	int value=0;
	public void change(int value) {
		this.value = value;
	}
	
}
