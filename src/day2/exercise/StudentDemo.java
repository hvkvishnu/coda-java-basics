package day2.exercise;

public class StudentDemo {

	public static void main(String[] args) {
		Result result = new Result("vishnu",234,92,84,90);
		result.calculateMarks();
		result.display();
	}
}
abstract class Student{
	String name;
	int rno;
}
class Exam extends Student{
	int m1,m2,m3;
	public Exam() {
		
	}
	public Exam(int x, int y, int z) {
		m1 = x;
		m2 = y;
		m3 = z;
	}
}
class Result extends Exam{
	int totalMark;
	public Result(String name,int rno,int x,int y,int z) {
		super(x,y,z);
		this.name = name;
		this.rno = rno;
	}
	public void calculateMarks(){
		totalMark = this.m1 + this.m2 + this.m3;
	}
	public void display(){
		System.out.println("Student name : "+ this.name);
		System.out.println("Student roll.no : "+ this.rno);
		System.out.println("Total mark is..."+totalMark);
		
	}
}
