package day2.exercise;

public class WiproDemo {
	public static void main(String[] args) {
		Wipro[] wipro ={new WiproTech(),new WiproInfoTech(),new WiproBPO()};
		wipro[0].showCompanyName();
		wipro[1].showCompanyName();
		wipro[2].showCompanyName();
		
		//NoObj noObj = new NoObj();
	}
}
abstract class Wipro{
	abstract public void showCompanyName();
}
class WiproTech extends Wipro{
	@Override
	public void showCompanyName() {
		System.out.println("Welcome Wipro Technologies...");	
	}
}
class WiproInfoTech extends Wipro{
	@Override
	public void showCompanyName() {
		System.out.println("Welcome Wipro Info Technologies");	
	}
}
class WiproBPO extends Wipro{
	@Override
	public void showCompanyName() {
		System.out.println("Welcome Wipro BPO....");
	}
}

abstract class NoObj{
	
}

