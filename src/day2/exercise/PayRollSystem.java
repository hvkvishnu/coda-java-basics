package day2.exercise;

public class PayRollSystem {

	public static void main(String[] args) {
		SalesPerson sp = new SalesPerson("Babu", "11.02.1998");
		sp.calculateSalary();
		sp.printDetails();
		SalesManager sm = new SalesManager("Ramesh", "10.10.1995");
		sm.calculateSalary();
		sm.printDetails();
		SalesTeritory st = new SalesTeritory("Somu", "02.03.2002");
		st.calculateSalary();
		st.printDetails();
		Worker wk = new Worker("ramu", "05.11.2011");
		wk.calculateSalary();
		wk.printDetails();
		
	}

}
class Date{
	String date;
	public Date(String date) {
		this.date = date;
	}
	public String getDate(){
		return this.date;
	}
}
abstract class Employee{
	String empName;
	double salary;
	Date doj;
	public void printDetails(){
		System.out.println("Employee Name : "+this.empName);
		System.out.println("Employeee Salary : "+this.salary);
		System.out.println("date of joinng..: "+this.doj.getDate());
	}
	
	abstract public void calculateSalary();
	
}

class SalesPerson extends Employee{
	public SalesPerson(String name,String doj) {
		this.empName = name;
		this.doj = new Date(doj);
	}
	
	@Override
	public void calculateSalary() {
		this.salary = 23000*1.5;
	}
}

class SalesManager extends Employee{
	public SalesManager(String name,String doj) {
		this.empName = name;
		this.doj = new Date(doj);
	}
	
	@Override
	public void calculateSalary() {
		this.salary = 50000*0.6;
	}
}

class SalesTeritory extends Employee{
	public SalesTeritory(String name,String doj) {
		this.empName = name;
		this.doj = new Date(doj);
	}
	
	@Override
	public void calculateSalary() {
		this.salary = 33000*0.8;
	}
}

class Worker extends Employee{
	public Worker(String name,String doj) {
		this.empName = name;
		this.doj = new Date(doj);
	}
	
	@Override
	public void calculateSalary() {
		this.salary = 11000*0.5;
	}
}
