package day2.exercise;

public class SingletonDemo {

	public static void main(String[] args) {
		Singleton.createObj();
		Singleton.createObj();
		Singleton.createObj();
	}
}

class Singleton{
	private static Singleton single;
	private Singleton(){
		System.out.println("Singleton object created.....");
	}
	public static Singleton createObj(){
		if(single==null){
			single = new Singleton();
		}
		System.out.println("createObj method called....");
		return single;
	}
}
