package day2.exercise;

class Parent{
	public Parent() {
		System.out.println("Parent's constructor....");
	}
	
	static{
		System.out.println("Parent's static block...");
	}
	
	{
		System.out.println("Parent's initialization block....");
	}
}

class Child extends Parent{
	public Child() {
		System.out.println("Chlid's constructor....");
	}
	
	static{
		System.out.println("Chlid's static block...");
	}
	
	{
		System.out.println("Chlid's initialization block....");
	}
}
public class OrderOfInitialization {
	public static void main(String[] args) {
		System.out.println("Inside main");
		new Child();
	}
}
