package day2.exercise;

public class CountObj {

	public static void main(String[] args) {
		Obj obj1 = new Obj();
		System.out.println(Obj.count);
		Obj obj2 = new Obj();
		System.out.println(Obj.count);
	}

}
class Obj{
	public static int count = 0;
	public Obj() {
		count++;
	}
	public int getCount(){
		return count;
	}
}