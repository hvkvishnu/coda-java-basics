package day2.exercise;

public class FinalRefChange {

	public static void main(String[] args) {
		final int value = 5;
		// value = 10; we cannot change the value of final variable
		final CountOne countOne = new CountOne();
		// this changes possible
		System.out.println("Increment counter... "+countOne.counter());
		System.out.println("Increment counter... "+countOne.counter());
		//but cannot change the countOne reference
		//countOne = new CountOne();
	}
}

class CountOne{
	int count = 0;
	public int counter(){
		count++;
		return count;
	}
}