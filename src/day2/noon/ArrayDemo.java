package day2.noon;

public class ArrayDemo {

	public static void main(String[] args) {
		//single dimension
		String s[] = new String[5];
		s[0]= "vishnu";
		s[1]= "kumar";
		for(int i=0;i<s.length;i++){
			System.out.println(s[i]);
		}
		
		int i[] = new int[5];
		i[4] = 120;
		for(int ii : i){
			System.out.println(ii);
		}
		
		//multi dimension
		String ss[][] = new String[4][5];
		for(int j=0; j<ss.length;j++){
			for(int k =0; k<ss[j].length; k++){
				System.out.print(ss[j][k]+"   ");
			}
			System.out.println();
		}
	}

}
