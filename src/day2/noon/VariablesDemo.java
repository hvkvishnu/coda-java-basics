package day2.noon;

public class VariablesDemo {
	public static void main(String[] args) {
		
		//string
		//byte,short,int,char
		//float,double
		//boolean
		
		String str = "hello world";
		String str2 = new String("Hello world 2");
		
		byte b = 120;
		char c = 'a'; char cc = 65;
		int i = 1000;
		long lo = 1010101011;
		float f = 1.2f;
		double d = 1.33;
		double dd = f;
		
		
		//wrapper classes
		Character ch = new Character('c');
		Double dob = new Double(29292.29292);
		Long lon = new Long(2121212);
		Integer in = new Integer(1222);
		Boolean bool = Boolean.FALSE;
		
		//Type casting
		int num = b;
		byte by = (byte)num; //when u do higher to lower casting, ulose the precision 
		 
		//convert string to number
		 String strnum = "2020";
		 int numm = Integer.parseInt(strnum);
		 
		 //concatenate - never use String
		 //instead make use of StringBuffer or StringBuilder
		 //StringBuffer - old class - proir to jdk5 - thread safe - slow
		 //StringBuilder - new class - introduced in jdk5 - non-thread safe - fast
		 StringBuilder sbl = new StringBuilder("hello");
		 sbl.append("world");
		 StringBuffer sbf = new StringBuffer("hello");
		 sbf.append("wold");
		 
		 String sf = String.format("The String value is..:%s and int value is %d", "hello world",200);
		 System.out.println(sf);
		 System.out.printf("The String value is..:%s and int value is %d", "hello world",200);
		 
		 System.out.printf("Integer : %d\n",15);
		 System.out.printf("Floating number with 3 decimal : %.3f\n",1.2344423);
		 System.out.printf("Floating number with 8 decimal : %.3f\n",1.2344423367);
		 
		 System.out.printf("%-12s%-12s%s\n", "column 1","column2","column 3");
	}
}
