package day4.noon;

public class AggregateDemo {

	public static void main(String[] args) {
		ShakthiSocket ss = new ShakthiSocket();
		HPPlug hp = new HPPlug();
		IndianAdapter ia = new IndianAdapter();
		//ss.roundPinHole(hp);
		ia.ap=hp;
		ss.roundPinHole(ia);
	}

}
abstract class IndianPlug{
	public abstract void roundPin();
}
class ShakthiPlug extends IndianPlug{
	@Override
	public void roundPin() {
		System.out.println("Indian plug working.....");
	}
}
abstract class IndianSocket{
	public abstract void roundPinHole(IndianPlug ip); 
}
class ShakthiSocket extends IndianSocket{
	@Override
	public void roundPinHole(IndianPlug ip) {
		ip.roundPin();
	}
}
abstract class AmericanPlug{
	public abstract void slabPin(); 
}
class HPPlug extends AmericanPlug{
	public void slabPin(){
		System.out.println("American Plug is working....");
	}
}
class IndianAdapter extends IndianPlug{
	AmericanPlug ap;
	@Override
	public void roundPin() {
		ap.slabPin();  
	}
}