package day4.morning;

import java.util.Scanner;

public class BadProgram2 {

	public static void main(String[] args) {
		GoodFan khaitan = new GoodFan();
		Scanner sc =new Scanner(System.in);
		while(true){
			System.out.println("Enter to call pull method");
			sc.next();
			khaitan.pull();
		}
	}

}


class BadFan{
	int state = 0;
	public void pull(){
		if(state==0){
			System.out.println("Switch on state...");
			state++;
		}
		else if(state==1){
			System.out.println("medium speed state....");
			state++;
		}
		else if(state==2){
			System.out.println("high speed state...");
			state++;
		}
		else if(state==3){
			System.out.println("switch off state....");
			state = 0;
		}
	}
}
//bidirectional association
//object state management

class GoodFan{
	State state = new SwitchOffState();
	public void pull(){
		state.execute(this);
	}
}
abstract class State{
	public abstract void execute(GoodFan f); 
}
class SwitchOffState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("Switch on state.....");
		f.state = new SwitchOnState();
	}
}
class SwitchOnState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("medium speed state.....");
		f.state = new MediumSpeedState();
	}
}class MediumSpeedState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("high speed state.....");
		f.state = new HighSpeedState();
	}
}class HighSpeedState extends State{
	@Override
	public void execute(GoodFan f) {
		System.out.println("switch off state.....");
		f.state = new SwitchOffState();
	}
}

