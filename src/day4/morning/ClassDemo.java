package day4.morning;

public class ClassDemo {

	public static void main(String[] args) throws Exception {
		//Test t = new Test();//static way of creating object
//		Class c = Class.forName("day4.morning.Test");
//		Object o = c.newInstance();
//		((Test) o ).work();
		
		TestParent o2 = (TestParent) Class.forName("day4.morning.Test").getConstructor(int.class).newInstance(8);
		o2.work();
		
	}
}
class TestParent{
	public void work(){
		
	}
}
class Test extends TestParent{
	public Test(){
		
	}
	public Test(int name){
		System.out.println("test object created....");
	}
	public void work(){
		System.out.println("work method of test called....");
	}
}
