package day4.morning;

import java.util.Scanner;

public class BadProgram {

	public static void main(String[] args)  throws Exception{
		Child child = new Child();
		Dog tiger = new Dog();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter item name");
		String itemClass = sc.nextLine();
		
		Item item = (Item) Class.forName(itemClass).newInstance();
		child.playWithDog(tiger, item);
	}

}

class Dog{
	public void play(Item item){
//		if(item.equals("stick")){
//			System.out.println("you beat I bite...");
//		}
//		else if(item.equals("stone")){
//			System.out.println("you hit I....");
//		}
//	
		item.execute();
	}
}
abstract class Item{
	public abstract void execute();
}

class Stick extends Item{
	public void execute(){
		System.out.println("you beat I bite...");
	}
}

class Stone extends Item{
	public void execute(){
		System.out.println("you hit I....");
	}
}
class Child{
	public void playWithDog(Dog dog,Item item){
		dog.play(item);
		
	}
}