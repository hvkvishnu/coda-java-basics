package day11.morning;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

public class ColDemo3 {

	public static void main(String[] args) {
		MapDemo md = new MapDemo();
		md.demoMap();
		
		Map<Employee,String> emap = new TreeMap<>(new MyMapComparator());
		emap.put( new Employee("kumar",45),"kumar");
		emap.put(new Employee("siva",25),"siva");
		emap.put(new Employee("ram",30),"ram");
		
		Set<Map.Entry<Employee,String>> set = emap.entrySet();
		Iterator<Map.Entry<Employee,String>> iter = set.iterator();
		
		while(iter.hasNext()){
			Map.Entry<Employee,String> me = iter.next();
			System.out.println(me.getKey().name+":"+me.getKey().age+":"+me.getValue());
		}
		
	}
}
class MapDemo{
	public MapDemo() {
		System.out.println("....MAP DEMO....");
	}
	
	public void demoMap(){
		Map<String, Integer> map = new HashMap<>();
		map.put("ramu", 25);
		map.put("somu", 30);
		map.put("vijay", 18);
		
		System.out.println(map);
		
		System.out.println(map.get("somu"));
		
		Set<Map.Entry<String, Integer>> set = map.entrySet();
		Iterator<Map.Entry<String, Integer>> iter = set.iterator();
		
		while(iter.hasNext()){
			Map.Entry<String, Integer> me = iter.next();
			System.out.println(me.getKey()+":"+me.getValue());
		}
	}
}
class MyMapComparator implements Comparator<Employee>{
	@Override
	public int compare(Employee o1, Employee o2) {
		return o1.name.compareTo(o2.name);
	}
}
