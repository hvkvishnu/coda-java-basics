package day11.morning;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

public class ColDemoDict {

	public static void main(String[] args) {
		Dictionary<String, Integer> dict = new Hashtable<>();
		dict.put("pepsi", 25);
		dict.put("coke", 20);
		dict.put("fainta", 30);
		Enumeration e = dict.elements();
		while(e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
		e=dict.keys();
		while(e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
	}
}
