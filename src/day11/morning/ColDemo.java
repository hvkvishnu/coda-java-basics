package day11.morning;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;
import java.util.Vector;

public class ColDemo {

	public static void main(String[] args) {
		//vector
		VectorDemo vd = new VectorDemo();
		vd.demo();
		
		//stack
		StackDemo sd = new StackDemo();
		sd.demoStack();
		
		List<String> list = new LinkedList<>();
		List<String> list1 = new ArrayList<>();
		list .add("aaa");
		list.add("bbb");
//		System.out.println(list.get(0));
		System.out.println("......LIST DEMO.....");
		for(int i=0;i<list.size();i++){
			System.out.println(list.get(i));
		}
		
		for(String s : list){
			System.out.println(s);
		}
		
		Iterator<String> iter = list.iterator();
//		list.add("ccc");
		while(iter.hasNext()){
			System.out.println(iter.next());
		}
		
		ListIterator<String> listIter = list.listIterator();
		while(listIter.hasPrevious()){
			System.out.println("...prev:"+listIter.previous());
		}
	}
}

class VectorDemo{
	public VectorDemo() {
		System.out.println("........Vector demo....".toUpperCase());
	}
	public void demo(){
		Vector<String> vec = new Vector<>();
		vec.add("vishnu");
		vec.add("vijay");
		
		Enumeration<String> em = vec.elements();
		vec.add("kumar");
		while(em.hasMoreElements()){
			System.out.println(em.nextElement());
		}
	}
}

class StackDemo{ 
	public StackDemo() {
		System.out.println("........STACK DEMO.....");
	}
	public void demoStack(){
		Stack<Integer> stack = new Stack<>();
		stack.push(5);
		stack.push(6);
		stack.push(7);
		//peek element...
		System.out.println("Top element in stack..."+stack.peek());
		//search on stack...
		System.out.println("search for 6..is at index..:"+stack.search(6));
		//stack contains
		System.out.println("is contains 9.."+stack.contains(9));
		while(!stack.isEmpty()){
			System.out.println("popping.....:"+stack.pop());
		}
		System.out.println("After pop..."+stack);
		
	}
}