package day11.morning;

import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

public class ColDemoQueue {

	public static void main(String[] args) {
		Queue<Integer> queue = new LinkedList<>();
		queue.add(10);
		queue.add(20);
		queue.add(30);
		
		System.out.println(queue);
		System.out.println(queue.isEmpty());
		while(!queue.isEmpty())
			System.out.println(queue.poll());
		
		Properties pro = new Properties();
		pro.put("vishnu", "vishnu@gmail.com");
		pro.put("coda", "coda@presidio.com");
		System.out.println(pro.getProperty("vishnu"));
	}
}
