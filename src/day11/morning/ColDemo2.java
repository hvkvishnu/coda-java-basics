package day11.morning;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ColDemo2 {

	public static void main(String[] args) {
		HashSetDemo hd = new HashSetDemo();
		
		Set<String> st = new HashSet<>();
		hd.demoSet(st,"HashSet");
		
		Set<String> ts = new TreeSet<>((String o1, String o2)->{return o2.compareTo(o1);});
//		Set<String> ts = new TreeSet<>(new MyComparator());
		hd.demoSet(ts,"TreeSet");
		
		Set<Employee> emp = new TreeSet<>(new MyComparator());
		emp.add(new Employee("ramu", 20));
		emp.add(new Employee("somu", 25));
		
		for(Employee e : emp){
			System.out.println(e.name+":"+e.age);
		}
	}
}

class HashSetDemo{
	public HashSetDemo() {
		System.out.println(".....SET DEMO......");
	}
	public void demoSet(Set<String> st,String msg){
		//Set<String> st = new HashSet<>();
		System.out.println("////////"+msg+"////////");
		st.add("aaa");
		st.add("zzz");
		st.add("eee");
		st.add("aaa");
		System.out.println("Set :"+st);
		
		System.out.println("Size of set..:"+st.size());
		Iterator<String> iter = st.iterator();
		while(iter.hasNext()){
			System.out.println("Set element..:"+iter.next());
		}
	}
}
class MyComparator implements Comparator<Employee>{
	@Override // change to descending order
	public int compare(Employee o1, Employee o2) {
		return o2.compareTo(o1);
	}
}
class Employee implements Comparable<Employee>{
	String name;
	int age;
	public Employee(String name,int age) {
		this.name = name;
		this.age = age;
	}
	@Override
	public int compareTo(Employee o) {
		return this.name.compareTo(o.name);
	}
	
}