package day11.morning;

import java.util.HashMap;
import java.util.WeakHashMap;

public class ColDemoWeakHash {

	public static void main(String[] args) {
		WeakHashMap<Key, String> wm = new WeakHashMap<>();
		Key a1 = new Key("a1");
		Key a2 = new Key("a2");
		
		HashMap<Key, String> hm = new HashMap<>();
		hm.put(a1, "a1");
		hm.put(a2, "a2");
		
		wm.put(a1, "hello");
		wm.put(a2, "hai");
		
		System.out.println("In weakHm :"+wm);
		System.out.println("In Hm :"+hm);
		a1 = null;
		System.gc();
		
		System.out.println("In weakHm :"+wm);
		System.out.println("In Hm :"+hm);
		
	}
}
class Key{
	String id;
	public Key(String id) {
		this.id = id;
	}
}
