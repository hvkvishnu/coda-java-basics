package day11.noon;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class DateDemo {

	public static void main(String[] args) {
		LocalDate ld = LocalDate.now();
		System.out.println("Current date:"+ld);
		System.out.println("Pls one week:"+ld.plus(1,ChronoUnit.WEEKS));
		System.out.println("pls one decade:"+ld.plus(1,ChronoUnit.DECADES));
		
		
		LocalTime lt = LocalTime.now();
		System.out.println("current time:"+lt);
		
		Duration onehr = Duration.ofHours(1);
		LocalTime ltf = lt.plus(onehr);
		System.out.println(ltf);
		Duration diff = Duration.between(lt, ltf);
		System.out.println("Difference :"+diff);
		
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);
		System.out.println(ldt.toLocalDate());
		Month month = ldt.getMonth();
		int day = ldt.getDayOfMonth();
		int hours = ldt.getHour();
		System.out.println(month+":"+day+":"+ldt.getDayOfWeek());
	}
}
