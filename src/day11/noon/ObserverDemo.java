package day11.noon;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ObserverDemo {

	public static void main(String[] args){
		FireAlarm shakthi = new FireAlarm();
		Students coda = new Students();
		Teacher shohib = new Teacher();
		shakthi.addObserver(shohib);
		shakthi.addObserver(coda);
		shakthi.setFire();
	}
}
class ThreadedObservable extends Observable{
//	public void runThread(){
//		new Thread(()->{
//			Students coda = new Students();
//			this.addObserver(coda);
//			setChanged();
//			notifyObservers("Fire in the mountain run.....");
//			this.deleteObserver(coda);
//		}).start();
//		
//		new Thread(()->{
//			Teacher shohib = new Teacher();
//			this.addObserver(shohib);
//			setChanged();
//			notifyObservers("Fire in the mountain run.....");
//		}).start();
//	}
	
	ArrayList<Observer> list = new ArrayList<>();
	@Override
		public void notifyObservers(Object arg) {
			for(Observer o : list){
				new Thread(()->{
					o.update(this, arg);
				}).start();
			}
		}
	@Override
		public synchronized void addObserver(Observer o) {
			list.add(o);
		}
}
class FireAlarm extends ThreadedObservable{
	public void setFire(){
		setChanged();
		notifyObservers("Fire in the mountain run.....");
	}	
}
class Students implements Observer{
	@Override
	public void update(Observable o, Object msg) {
		System.out.println((String)msg);
		runStudent();
	}
	
	public void runStudent(){
		System.out.println("Students running....");
	}
}
class Teacher implements Observer{
	@Override
	public void update(Observable o, Object arg) {
		System.out.println((String)arg);
		System.out.println("I had back pain I'm thinking.....to run....");
		try{Thread.sleep(3000);}catch(Exception e){}
		runTeacher();
	}
	public void runTeacher(){
		System.out.println("Teacher running......");
	}
}