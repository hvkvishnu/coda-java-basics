package day11.noon;

import java.util.Random;

public class RandomDemo {
	public static void main(String[] args) {
		Random r = new Random();
		double d;
		for(int i=0;i<10;i++){
			d = r.nextGaussian();
			System.out.print(Math.round(d)+"\t");
		}
		System.out.println();
		for(int i=0;i<10;i++){
			System.out.print(r.nextInt(100)+"\t");
		}
		
		double x=30;double y=3;
		System.out.println(Math.max(x, y));
		
		System.out.println(Math.pow(3, 2));
		
	}
}
