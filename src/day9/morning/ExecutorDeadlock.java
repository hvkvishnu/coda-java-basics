package day9.morning;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorDeadlock {

	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2);
		Frog frog = new Frog();
		Crane crane = new Crane();
		es.execute(()->{
			crane.eat(frog);
		});
		es.execute(()->{
			frog.escape(crane);
		});
		es.shutdown();
	}
}
