package day9.morning;

public class OneObjectTwoThreadOneJob {
	//Inter thread communication
	public static void main(String[] args) {
		Gun bofors = new Gun();
		new Thread(()->{
			for(int i=0;i<5;i++){
				bofors.fillGun();
			}
		}).start();
	
		new Thread(()->{
			for(int i=0;i<5;i++){
				bofors.shootGun();
			}
		}).start();
	}
}
class Gun{
	boolean flag;
	synchronized public void fillGun(){
		if(flag){			
			try{wait();}catch (Exception e) {}
		}
		System.out.println("fill the gun...");
		flag = true;
		notify();
	}
	synchronized public void shootGun(){
		if(!flag){
			try{wait();}catch (Exception e){}
		}
		System.out.println("shoot the gun");
		flag = false;
		notify();
	}
}
