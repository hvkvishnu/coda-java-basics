package day9.morning;

public class ThreadDemo3 {

	public static void main(String[] args) {
		new Thread(()->{new Test().met();}).start();
		System.out.println("main thread...");
	}
}
class Test{
	public void met(){
		System.out.println("Test child....");
		try{
			Thread.sleep(5000);
		}catch(Exception e){}
	}
}
