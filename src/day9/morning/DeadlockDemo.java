package day9.morning;

public class DeadlockDemo {

	public static void main(String[] args) {
		Crane crane = new Crane();
		Frog frog = new Frog();
		
		new Thread(()->{
			//crane.catchFrog(frog);
			crane.eat(frog);
		}).start();
		
		new Thread(()->{
			frog.escape(crane);
//			frog.catchCrane(crane);
		
		}).start();
	}
}
class Frog{
	synchronized public void escape(Crane c){
		c.cranesLeaveMethod();
	}
	synchronized public void fogsLeaveMethod(){
		
	}
}
class Crane{
	synchronized public void eat(Frog f){
		System.out.println("eating.....");
		System.out.println("eating.....");
		f.fogsLeaveMethod();	
		System.out.println("swahaaa....");
	}
	synchronized public void cranesLeaveMethod(){
		
	}
}

//My experiments
//class Crane{
//	boolean craneCatch = false;
//	synchronized public void catchFrog(Frog f){
//		f.frogCatch = true;
//		System.out.println("Crane catches frog....");
//		eatFrog();
//	}
//	synchronized public void eatFrog(){
//		if(craneCatch){
//			System.out.println("Crane: ohhh....frog got my neck...can't eat...");
//			System.out.println("Crane: waiting for frog to leave...");
//		}else{
//			System.out.println("Crane: Yummyyy....");
//		}
//	}
//}
//class Frog{
//	boolean frogCatch = false;
//	synchronized public void catchCrane(Crane c){
//		c.craneCatch = true;
//		System.out.println("Frog catches crane...");
//		killCrane();
//	}
//	synchronized public void killCrane(){
//		if(frogCatch){
//			System.out.println("Frog: I'm stuck inside crane's neck...can't kill...");
//			System.out.println("Frog: waiting for crane to leave...");
//		}else{
//			System.out.println("Frog: killed....");
//		}
//	}
//}
