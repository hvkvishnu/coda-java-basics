package day9.noon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorOTOB {

	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		es.execute(()->{
			Resource res = Factory.getResource();
			res.name = "Executor first resource string";
			Resource r1 = Factory.getResource();
			System.out.println(r1.name);
			Resource r2 = Factory.getResource();
			System.out.println(r2.name);
			Factory.removeResourceFromThread();
			Resource r3 = Factory.getResource();
			System.out.println(r2.name);
		});
		
		es.execute(()->{
			Resource resource = Factory.getResource();
		});
		es.shutdown();
	}
}
