package day9.noon;

public class OneThreadOneObject {

	public static void main(String[] args) {
		new Thread(()->{
			Resource res = Factory.getResource();
			res.name = "first time setting name";
			Resource re = Factory.getResource();
			System.out.println(re.name);
			Resource r1 = Factory.getResource();
			System.out.println(r1.name);
			Factory.removeResourceFromThread();
			Resource r4 = Factory.getResource();
		}).start();
		
		new Thread(()->{
			Resource resource = Factory.getResource();
			System.out.println("in second thread res.name "+resource.name);
		}).start();
	}
}

class Factory{
	private static ThreadLocal tlocal = new ThreadLocal();
	public static Resource getResource(){
		Resource r = (Resource) tlocal.get();
		if(r == null){
			r = new Resource();
			tlocal.set(r);
		}
		return r;
	}
	
	public static void removeResourceFromThread(){
		if(tlocal.get() != null){
			tlocal.remove();
			System.out.println("resource removed...");
		}
	}
}
class Resource{
	String name;
	public Resource() {
		System.out.println("Resource cons called....");
	}
}
