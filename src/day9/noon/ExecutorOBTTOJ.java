package day9.noon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorOBTTOJ {

	public static void main(String[] args) {
		ExecutorService es = Executors.newFixedThreadPool(2);
		Gun gun = new Gun();
		es.execute(()->{
			for(int i=0;i<5;i++){
				gun.fillGun();
			}
		});
		es.execute(()->{
			for(int i=0;i<5;i++){
				gun.shootGun();
			}
		});
		es.shutdown();
	}
}
class Gun{
	boolean flag;
	synchronized public void fillGun(){
		if(flag){			
			try{wait();}catch (Exception e) {}
		}
		System.out.println("fill the gun...");
		flag = true;
		notify();
	}
	synchronized public void shootGun(){
		if(!flag){
			try{wait();}catch (Exception e){}
		}
		System.out.println("shoot the gun");
		flag = false;
		notify();
	}
}
