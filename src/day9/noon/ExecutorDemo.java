package day9.noon;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {

	public static void main(String[] args) throws Exception{
		ExecutorService es = Executors.newFixedThreadPool(3);
		//implements runnable
		es.execute(()->{
			System.out.println("child thread...");
		});
		es.execute(()->{
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			System.out.println("child thread 2...");
		});
		
		System.out.println("main thread...");
		//implements callable
		Future future =  es.submit(()->{
			return "callable string";
		});
		
		System.out.println("the value is :"+future.get());
		es.shutdown();
	}
}
