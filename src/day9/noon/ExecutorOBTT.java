package day9.noon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorOBTT {

	public static void main(String[] args) {
		 ReservationCounter counter = new ReservationCounter();
		 ExecutorService es = Executors.newFixedThreadPool(2);
		 es.execute(()->{
			 synchronized (counter) {
				 Thread.currentThread().setName("ramu");
				 counter.bookTicket(1000);
				 counter.giveChange();
			}
			//System.out.println(es.toString());
		 });
		 es.execute(()->{
			 synchronized (counter) {
				 Thread.currentThread().setName("somu");
				 counter.bookTicket(500);
				 counter.giveChange();
			}
		 });
		 System.out.println(es);
		 es.shutdown();	 
	}
}
class ReservationCounter{
	int amt;
	synchronized public void bookTicket(int amt){
		this.amt = amt;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Ticket booked for.."+name+" and amout paid is.."+amt);
		//try{Thread.sleep(5000);}catch (Exception e) {}
	}
	synchronized public void giveChange(){
		int change = amt-100;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Change given to.."+name+" and change given is.."+change);
	}
}
