package day8.morning;

public class MethodRefDemo {
	public static void main(String[] args) {
		Func1 fadd = new AddBazzar()::add;
		int sum =fadd.add(5, 10);
		System.out.println("Sum is :"+sum);
		
		Func1 faddstat =AddBazzar::addStat;
		sum = faddstat.add(10,20);
		System.out.println("sum is :"+sum);
	}
}
interface Func1{
	public int add(int i,int j);
}
class AddBazzar{
	public int add(int i,int j){
		return i+j;
	}
	public static int addStat(int i, int j){
		return i+j;
	}
}
