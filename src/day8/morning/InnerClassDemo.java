package day8.morning;

public class InnerClassDemo {

	public static void main(String[] args) {
//		KaliMark kali = new KaliMark();
//		kali.makeBovonto();
		
		Pepsi pep = new Pepsi();
		pep.makePepsi();
	}
}
abstract class Cola{ // functional abstract class / interface
	public abstract void makeCola();
}
interface ColaInter{
	public void makeCola();
}
interface ColaInter2{
	public void makeCola(int quantity);
}
interface Add{
	public int add(int i,int j);
}
class Pepsi{
	public void makePepsi(){
//		class CampaCola extends Cola{ //inner class inside method not able to used by its member
//			@Override
//			public void makeCola() {
//				System.out.println("cola made by campa cola....");
//			}
//		}
		
		new Cola(){	//anonymous inner classes 
			@Override
			public void makeCola() {
				System.out.println("cola made....");
			}
		}.makeCola();
//		Cola cola = new CampaCola();
//		cola.makeCola();
		System.out.println("fill in pepsi bottle sell...");
		
		//lambda syntax...
		ColaInter cl = () ->{
			System.out.println("making cola....");
		};
		cl.makeCola();
		
		//with arguments
		ColaInter2 ci = (value)->{
			System.out.println("making cola of liters :"+value);
		};
		ci.makeCola(500);
		
		//another example
		Add add = (i,j)->{
			return i+j;
		};
		addNumbers(10, 40, add);
	}
	public void addNumbers(int i, int j,Add add){
		System.out.println("Addition of i and j is :"+add.add(i,j));
	}
//	class CampaCola extends Cola{	//inner class
//		@Override
//		public void makeCola() {
//			System.out.println("cola made by campa cola....");
//		}
//	}
	
//	public Cola trojan(){
//		return new CampaCola();
//	}
}
class KaliMark{
	public void makeBovonto(){
		//Cola cola = new Pepsi().new CampaCola();
		//cola.makeCola();
		System.out.println("fill in bovonto bottle sell.");
	}
}