package day8.noon;

public class ThreadDemo {

	public static void main(String[] args) {
		Thread t = new Thread(()->{System.out.println("child thread called....");});
		t.start();
		new Thread(new MyRunable2()).start();
		System.out.println("main thread called...");
	}
}
class MyRunable2 implements Runnable{
	@Override
	public void run() {
		System.out.println("child.....");
	}
}
