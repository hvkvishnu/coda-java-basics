package day8.noon;

public class SingleThreadDemo {

	public static void main(String[] args) throws Exception{
		Thread t = Thread.currentThread();
		t.setName("bot");
		System.out.println(t);
		for(int i=0;i<5;i++){
			System.out.println(i);
			Thread.sleep(1000);
			//System.exit(1);
		}
		//how to span a thread
		Thread t2 = new Thread(()->{System.out.println("child thread...lambda");});
		t2.run();
		Thread t3 = new Thread(new MyRunnable());
		t3.run();
				new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("child thread....anonymous inner class");
			}
		}).run();
	}
}
class MyRunnable implements Runnable{
	@Override
	public void run() {
		System.out.println("chlid thread....traditional way");
	}
}
