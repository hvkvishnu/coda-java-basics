package day10.noon;

public class EnumDemo {
	enum Directions{
		NORTH(120),SOUTH,EAST,WEST;
		
		private int degree;
		private Directions() {}
		private Directions(int degree) {
			this.degree = degree;
		}
	}
	enum Operation{
		ADD{
			@Override
			int execute(int a, int b) {
				return a+b;
			}
		},
		SUB{
			@Override
			int execute(int a, int b) {
				return a-b;
			}
		};
		abstract int execute(int a,int b);
		
	}

	public static void main(String[] args) {
		Operation op = Operation.valueOf("ADD");
		System.out.println(op.execute(5, 45));
		
		op = Operation.valueOf("SUB");
		System.out.println(op.execute(5, 45));
		
		Directions north = Directions.NORTH;
		System.out.println("North :"+north.degree);
		
		Directions south = Directions.SOUTH;
		System.out.println("South :"+south.degree);
		
		
	}
}
