package day10.morning;

public class GenericsDemo {

	public static void main(String[] args) {
		PaintBrush<Paint> pb = new PaintBrush<>();
		pb.setObj(new RedPaint());
		Paint paint = pb.getObj();
		paint.doPaint();
		
		PaintBrush<Dry> pd = new PaintBrush();
		pd.setObj(new DryAir());
		Dry dry = pd.getObj();
		dry.gijiGija();
	}
}
class PaintBrush<T>{
	private T obj;
	
	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
}
abstract class Paint{
	abstract void doPaint();
}
class RedPaint extends Paint{
	@Override
	void doPaint() {
		System.out.println("red color.....");
	}
}
abstract class Liquid{
	abstract void spray();
}
class ColorLiquid extends Liquid{
	@Override
	void spray() {
		System.out.println("hey.....holi..holii");
	}
}
abstract class Dry{
	abstract void gijiGija();
}
class DryAir extends Dry{
	@Override
	void gijiGija() {
		System.out.println("I play hehe...hehe...");
	}
}