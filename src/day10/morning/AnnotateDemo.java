package day10.morning;

import java.util.ArrayList;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
@FunctionalInterface
interface x
{
	public void y();
	
}
//marker annotation
//single valued annot
//multi valued annot
//meta annot
@Inherited
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)//on wat level we are using this anot
@interface newModel
{
	String s() default "old benz";
	int version() default 3;
}
@newModel(s="new benz",version=4)
class benz
{
	String m;
	int v;
	benz(String s,int v)
	{
		this.m=s;
		this.v=v;
	}
	
}
class A
{
	public void myClassFunc()
	{
		System.out.println("clas A");
	}
	
}
class AnnotateDemo extends A
{
    @SuppressWarnings("unchecked")
	 @Override
	public void myClassFunc()
	{
		System.out.println("clas annotations");
		ArrayList<Integer> obj=new ArrayList<Integer>();
		obj.add(1);
	}
    @Deprecated
    public void show()
    {
    	
    	
    }
     public static void main(String args[])
     {
        AnnotateDemo obj = new AnnotateDemo();
         obj.myClassFunc();
         obj.show();//not to use
         benz Benz=new benz("benz1",5);
         //how to print value of the annot parameters
        // newModel s;
         Class c=Benz.getClass();
         Annotation an=c.getAnnotation(newModel.class);
         newModel n=(newModel)an;
         System.out.println(n.s());
     }
}