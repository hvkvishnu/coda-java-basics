package day1.exercise;

public class Quadratic {
	private int a,b,c;
	
	public Quadratic(){
		this.a = 1;
		this.b = 1;
		this.c = 1;
	}
	
	public Quadratic(int x, int y, int z){
		this.a = x;
		this.b = y;
		this.c = z;
	}
	
	public int getA(){
		return a;
	}
	
	public int getB(){
		return b;
	}
	
	public int getC(){
		return c;
	}
	
	public void modify(int x, int y, int z){
		this.a = x;
		this.b = y;
		this.c = z;
	}
	
	public int compute(int x){
		int result = a*a*x;
		result += b*x;
		result += c;
		return result;
	}

	public static void main(String[] args) {
		Quadratic qd = new Quadratic(4,5,6);
		System.out.println("Quadratic function : "+qd.getA()+"x"+" + "+qd.getB()+"x "+qd.getC());
		System.out.println("Modifying.....");
		qd.modify(10, 6, 40);
		System.out.println("Modified Quadratic function : "+qd.getA()+"x"+" + "+qd.getB()+"x "+qd.getC());
		System.out.println("Replace x with 3");
		System.out.println("Result... : "+qd.compute(3));
	}

}
