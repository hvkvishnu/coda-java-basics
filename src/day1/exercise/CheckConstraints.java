package day1.exercise;

import java.util.Scanner;

public class CheckConstraints {
	public void isArmstrong(int num){
		int n = num;
        int res=0,remainder;
        while(num > 0){
            remainder = num % 10;
            res = res + (remainder*remainder*remainder);

            num = num / 10;

        }

        if(res == n)
            System.out.println(n+" is an Armstrong Number");

        else

            System.out.println(n+" is not a Armstrong Number");		
	}
	
	public void isPalindrome(int num){
		 int k = num;
         int reverse=0,rem;
         while(num > 0){
             rem = num % 10;
             reverse = reverse * 10 + rem;
             num = num / 10;
         }
         if(reverse == k)
             System.out.println(k+" is a Palindrome Number");
         else
             System.out.println(k+" is not a Palindrome Number");
	}
	
	public void isPerfectNumber(int num){
		int perfectNo = 0;               
        int i;             
        for (i = 1; i < num; i++) {           
            if (num % i == 0) {                              
                perfectNo += i;           
            }         
        }            
        if (perfectNo == num) {  
            System.out.println("number is a perfect number");         
        }
        else
        {                      
            System.out.println("number is not a perfect number");    
        }  
	}

	public static void main(String[] args) {
		CheckConstraints cc = new CheckConstraints();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ");
		int num = sc.nextInt();
		
		cc.isArmstrong(num);
		cc.isPalindrome(num);
		cc.isPerfectNumber(num);
		

	}

}
