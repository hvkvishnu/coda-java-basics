package day1.exercise;

public class DefaultPrimitiveDataTypes {

	public static void main(String[] args) {
		//String
		String str = "hello world";
		System.out.println("String"+str);
		
		
		//Number
		int num = 1000;
		System.out.println("Integer "+ num);
		
		byte byt = 110;
		System.out.println("Byte "+ byt);
		
		char ch = 'a';
		System.out.println("Character "+ ch);
		
		long lng = 11001101;
		System.out.println("Long "+ lng);
		
		//Float and Double
		float flt = 3.1427f;
		double dbl = 3.142745;
		System.out.println("Float and Double "+ flt +"  "+ dbl);
		
		//Boolean
		boolean bool = true;
		System.out.println("Boolean "+ bool);
		
	}

}
