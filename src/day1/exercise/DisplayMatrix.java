package day1.exercise;

public class DisplayMatrix {

	public static void main(String[] args) {
		int ss[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		
		for(int j=0; j<ss.length;j++){
			for(int k =0; k<ss[j].length; k++){
				System.out.print(ss[j][k]+"   ");
			}
			System.out.println();
		}
	}
		
}
