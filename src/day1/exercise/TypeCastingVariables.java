package day1.exercise;

public class TypeCastingVariables {

	public static void main(String[] args) {
				//String
				String str = new String("hello world");
				System.out.println("String"+str);
				String str2 = str + ". Welcome Java";
				System.out.println("String"+str2);
				
				
				//Number
				int num = 1000;
				System.out.println("Integer "+ num);
				Integer num2 = new Integer(100);
				System.out.println("Integer "+ num2);
				
				byte byt = 110;
				System.out.println("Byte "+ byt);
				
				
				char ch = 'a';
				System.out.println("Character "+ ch);
				char c = 65;
				
				long lng = 11001101;
				System.out.println("Long "+ lng);
				Long lng2 = new Long(5445454);
				System.out.println("Long "+ lng2);
				
				//Float and Double
				float flt = 3.1427f;
				double dbl = 3.142745;
				double dbl2= flt;
				System.out.println("Float and Double "+ flt +"  "+ dbl+ " "+dbl2);
				
				//Boolean
				boolean bool = true;
				System.out.println("Boolean "+ bool);
				Boolean bool2 = Boolean.TRUE;
				System.out.println("Boolean "+bool2);
				
				//int to string type cast
				String newStr = str + num;
				System.out.println("int to string type cast" + newStr);
				
				//string to int
				int newInt = Integer.parseInt("5000");
				System.out.println("string to int "+newInt);
				
				//float to int
				int fltToInt = (int) flt;
				System.out.println("float to int "+fltToInt);
				
				//char to int
				int chrToInt = ch;
				System.out.println("char to int "+ chrToInt);
				
				
				
				
				
	}

}
