package day1.exercise;

import java.util.Scanner;

public class AverageOfNumbers {
	public double calculateAverage(int a[], int n){
		int total = 0;
		for(int i=0; i<n; i++){
			total += a[i];
		}
		return total/n;
	}

	public static void main(String[] args) {
		AverageOfNumbers obj = new AverageOfNumbers();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the count of numbers to be calculated");
		int n = sc.nextInt();
		System.out.println("Enter the Numbers");
		int[] arr = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		double result = obj.calculateAverage(arr, n);
		System.out.println("Average is : "+ result);
	}

}
