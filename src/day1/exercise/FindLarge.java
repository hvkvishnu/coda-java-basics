package day1.exercise;

import java.util.Scanner;

public class FindLarge {
	public int findLarge(int arr[]){
		int large = arr[0];
		for(int i : arr){
			if(i > large)
				large = i;
		}
		return large;
	}
	public static void main(String[] args) {
		FindLarge fl = new FindLarge();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no of rows");
		int n = sc.nextInt();
		System.out.println("Enter no of columns");
		int m = sc.nextInt();
		int arr[][] = new int[n][m];
		System.out.println("Enter values");
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		
		for(int i=0;i<n;i++){
			System.out.println("Largest number in row "+ (i+1) +" is "+ fl.findLarge(arr[i]));
		}
		
		
	}

}
