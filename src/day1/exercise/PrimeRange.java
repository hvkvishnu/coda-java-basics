package day1.exercise;

public class PrimeRange {

	public static void main(String[] args) {
		CheckPrime cp = new CheckPrime();
		int arr[] = new int[100];
		int n = 0;
		for(int i=2; i<= 100; i++){
			if(cp.isPrime(i)){
				arr[n] = i;
				n++;
			}
		}
		
		for(int i =0; i<n; i++){
			System.out.print(arr[i] + " ");
		}
	}

}
