package day1.exercise;

import java.util.Scanner;

public class ReverseArray {
	
	public void printArr(int a[]){
		System.out.println("Elements of array");
		for(int i : a){
			System.out.print(i+"  ");
		}
		System.out.println();
	}
	
	public void reverse(int[] arr){
		int mid = (arr.length-1) / 2;
		int len = arr.length;
		for(int i=0;i<=mid;i++){
			int temp = arr[i];
			arr[i]=arr[len-i-1];
			arr[len-i-1]=temp;
		}
	}
	
	public static void main(String[] args) {
		ReverseArray ra = new ReverseArray();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the count of numbers to be calculated");
		int n = sc.nextInt();
		System.out.println("Enter the Numbers");
		int[] arr = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Before reverse");
		ra.printArr(arr);
		System.out.println("After revese");
		ra.reverse(arr);
		ra.printArr(arr);
		
		
		
	}

}
