package day1.exercise;

import java.util.Scanner;

public class SortArray {
	public void printArr(int a[]){
		System.out.println("Elements of array");
		for(int i : a){
			System.out.print(i+"  ");
		}
		System.out.println();
	}
	
	public void sort(int[] arr){
		for(int i=0; i<arr.length; i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i] > arr[j]){
					int temp = arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("Sorting in Ascending order");
		SortArray sa = new SortArray();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the count of numbers to be calculated");
		int n = sc.nextInt();
		System.out.println("Enter the Numbers");
		int[] arr = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Before sort");
		sa.printArr(arr);
		sa.sort(arr);
		System.out.println("After sort");
		sa.printArr(arr);
	}

}
