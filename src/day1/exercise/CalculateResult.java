package day1.exercise;

import java.util.Scanner;

public class CalculateResult {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no of students");
		int n = sc.nextInt();
		System.out.println("Enter no of subjects");
		int m = sc.nextInt();
		int arr[][] = new int[n][m];
		for(int i=0;i<n;i++){
			System.out.println("Enter mark for student "+ (i+1));
			for(int j=0;j<m;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		
		
		for(int i=0;i<n;i++){
			int total = 0;
			System.out.println("Total for student "+ (i+1)+" "+total);
			for(int j=0;j<m;j++){
				total += arr[i][j];
			}
			double avg = total/3;
			System.out.println("Average for student "+(i+1)+" " +avg);
			
		}
		
	}
}
