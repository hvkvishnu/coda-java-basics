package day1.exercise;

import java.util.Scanner;

public class Occurence {
	public int findOccurence(int[] arr, int idx,int key){
		int occur = 1;
		for(int i=idx;i < arr.length; i++){
			if(arr[i] == key){
				occur++;
			}
		}
		return occur;
	}

	public static void main(String[] args) {
		Occurence oc = new Occurence();
		LocationFinder lo = new LocationFinder();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter N");
		int n = sc.nextInt();
		System.out.println("Enter the Numbers");
		int[] arr = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter the element to search");
		int key = sc.nextInt();
		
		int index = lo.findIndex(arr, key);
		if(index != 1){
			System.out.println(key +" found");
			int result = oc.findOccurence(arr,index,key);
			System.out.println(key +" is occured "+result+" times");
		}
		else{
			System.out.println(key+" not found");
		}
	}

}
