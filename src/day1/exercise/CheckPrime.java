package day1.exercise;

import java.util.Scanner;

public class CheckPrime {
	
	public boolean isPrime(int num){
		if(num <= 1){
			return false;
		}
		
		for(int i =2; i < num; i++){
			if(num % i == 0){
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		CheckPrime checkPrime = new CheckPrime();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number... ");
		int number = sc.nextInt();
		if(checkPrime.isPrime(number)){
			System.out.println(number+" is prime");
		}
		else{
			System.out.println(number+" is not prime");			
		}

	}

}
