package day1.exercise;

import java.util.Arrays;

public class ResizeArray {
	public void printArr(int a[]){
		System.out.println("Elements of array");
		for(int i : a){
			System.out.print(i+"  ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5};
		ResizeArray re = new ResizeArray();
		System.out.println("Before resizing");
		re.printArr(arr);
		arr = Arrays.copyOf(arr, arr.length+5);
		System.out.println("After extending size to 10");
		re.printArr(arr);
		
	}

}
