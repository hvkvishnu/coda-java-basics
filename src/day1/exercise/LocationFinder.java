package day1.exercise;

import java.util.Arrays;
import java.util.Scanner;

public class LocationFinder {
	public int findIndex(int[] arr,int k){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==k){
				return i+1;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		LocationFinder lf = new LocationFinder();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter N");
		int n = sc.nextInt();
		System.out.println("Enter the Numbers");
		int[] arr = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter the element to search");
		int key = sc.nextInt();
		int position = lf.findIndex(arr,key);
		if(position == -1){
			System.out.println("Given position not in array");
		}
		else{
			System.out.println(key +" is found in the position "+ position);
		}
	}

}
