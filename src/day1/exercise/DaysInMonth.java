package day1.exercise;

import java.util.Scanner;

public class DaysInMonth {

	public static void main(String[] args) {
		int month;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Month Number ");
		month = sc.nextInt();	
		System.out.println("Enter year ");
		int year = sc.nextInt();
		switch(month)
		{
		  	case 1:
		  	case 3:
			case 5: 	
			case 7:
			case 8:
			case 10:
			case 12:			  	
				System.out.println("31 Days in this Month");
			  	break;
			
			case 4:	
			case 6:
			case 9:
			case 11:			    	
				System.out.println("30 Days in this Month");  
				break;
			
			case 2:
				if (((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0))
					System.out.println("29 Days in this Month");
				else
					System.out.println("28 Days in this Month");
				break;
			
			default:		  	
				System.out.println(" Please enter Valid Number between 1 to 12");
		  }
	}

}
