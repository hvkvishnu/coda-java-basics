package day1.exercise;

import java.util.Scanner;

public class ReverseTwoD {
	public void printArr(int[][] arr){
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		ReverseTwoD rtd = new ReverseTwoD();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no of rows");
		int n = sc.nextInt();
		System.out.println("Enter no of columns");
		int m = sc.nextInt();
		int arr[][] = new int[n][m];
		System.out.println("Enter values");
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		System.out.println("Before reverse");
		rtd.printArr(arr);
		ReverseArray re = new ReverseArray();
		for(int i=0;i<n;i++){
			re.reverse(arr[i]);
		}
		System.out.println("After reverse");
		rtd.printArr(arr);
	}

}
