package day3.noon;
import static org.junit.Assert.*;

import org.junit.Test;
public class SingletonTest {
	
	public static SingleTon createObj(){
		return SingleTon.createSingleton();
	}
	
	@Test
	public void testSingleton(){
		SingleTon obj1 = createObj();
		SingleTon obj2 = createObj();
		
		assertSame("same object reffernce",obj1,obj2);
	}
}
