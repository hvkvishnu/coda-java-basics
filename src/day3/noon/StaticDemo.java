package day3.noon;

public class StaticDemo {

	public static void main(String[] args) {
		SingleTon st1 = SingleTon.createSingleton();
		SingleTon st2 = SingleTon.createSingleton();
		
		//SingleTon st3 = new SingleTon(); not possible because of private constructor
	}
}

class SingleTon{
	private static SingleTon single;
	private SingleTon() {
		System.out.println("Singleton object created... ");
	}
	
	public static SingleTon createSingleton(){
		if(single==null)
			single = new SingleTon();
		return single;
	}
}
