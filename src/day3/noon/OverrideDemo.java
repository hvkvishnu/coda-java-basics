package day3.noon;

public class OverrideDemo {

	public static void main(String[] args) {
		Parent parent =  new GrandChild();
		//parent.met();
	}

}

class Parent{
	public Parent() {
		System.out.println("non parameterized parent...");
	}
	
	public Parent(int i){
		System.out.println("parameterized parent constructor");
	}
	
	void met(){
		System.out.println("met method in parent.....");
	}
}

class Child extends Parent{
	Child(){
		super(23);//must be at top of constructor
		System.out.println("Child constructor");
	}
	protected void met(){
		System.out.println("met method in child...");
		super.met();
	}
}
class GrandChild extends Child{
	public GrandChild() {
		System.out.println("Grand child...");
		super.met();
	}
}
//visibility cannot be reduced
//return type cannot be changed
//name and parameters cannot be changed
