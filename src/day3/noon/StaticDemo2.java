package day3.noon;

public class StaticDemo2 {

	public static void main(String[] args) {
		MuttonShop.sellMutton();
		MuttonShop.sellMutton();
		MuttonShop.sellMutton();
		MuttonShop.sellMutton();

	}

}

class MuttonShop{
	private MuttonShop(){
		
	}
	
	static{
		System.out.println("welcome to mutton shop....");
	}
	
	public static void sellMutton(){
		System.out.println("mutton sold.....");
	}
}