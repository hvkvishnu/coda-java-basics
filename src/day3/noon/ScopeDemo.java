package day3.noon;

public class ScopeDemo {
	static TrainingCenter tc = new TrainingCenter();
	static TrainingCenter tc2 = new TrainingCenter();
	public static void main(String[] args) {
		//tc.createTrainingRoom();
		TrainingCenter.createCanteen();
	}

}

class TrainingCenter{
	static Canteen canteen = new Canteen();//class variable
	TrainingRoom troom = new TrainingRoom();//instance variable
	
	static void createCanteen(){
		canteen = new Canteen();
		//TrainingRoom troom2 = new TrainingRoom();
	}
	
	void createTrainingRoom(){
		troom = new TrainingRoom();
	}
}

class TrainingRoom{
	public TrainingRoom() {
		System.out.println("Training room created");
	}
}

class Canteen{
	public Canteen() {
		System.out.println("canteen created");
	}
}