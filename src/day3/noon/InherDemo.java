package day3.noon;

public class InherDemo {

	public static void main(String[] args) {
		PaintBrush brush = new PaintBrush();
		brush.paint = new RedPaint();
		brush.doPaint();
		

	}

}

class PaintBrush{
	Paint paint;
	public void doPaint(){
		System.out.println(paint);
	}
}

class Paint{}
class RedPaint extends Paint{}
class Bluepaint extends Paint{}
class GreenPaint extends Paint{}
//STRATEGY PATTERN
//1.Delete if-else-if ladder
//2.Convert the condition to classes
//3.Group them under a hierarchy
//4.Create association b/w class and base class

