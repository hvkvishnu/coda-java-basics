package day3.morning;

public class AcessDemo {

	
	int nomod = 50;
	public int pub = 100;
	private int pri = 1000;
	protected int pro = 500;
	
	public void met(){
		System.out.println(pub);
		System.out.println(pri);
		System.out.println(pro);
		System.out.println(nomod);
	}
}

class Sub extends AcessDemo{
	public void met(){
		System.out.println(pub);
		//System.out.println(pri);
		System.out.println(pro);
		System.out.println(nomod);
	}
}

class NonSub{
	public void met(){
		AcessDemo obj = new AcessDemo();//association
		System.out.println(obj.pub);
		//System.out.println(obj.pri);
		System.out.println(obj.pro);
		System.out.println(obj.nomod);
	}
}
