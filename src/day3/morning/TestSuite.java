package day3.morning;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({OverLoadDemoTest.class,ReservationTestDemo.class})
public class TestSuite {
	
}
