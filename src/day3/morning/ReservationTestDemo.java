package day3.morning;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;;

public class ReservationTestDemo {
	Customer1 somu;
	
	@Before
	public void initializeCustomer(){
		somu = new Customer1();
	}
	@Test
	public void testAmount(){
		assertEquals("Money before deduct",1000, somu.getMoney());
		somu.deductMoney(700);
		assertEquals("Money after deduct",300, somu.getMoney());
	}
}
