package day3.morning;


import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class OverLoadDemoTest {
	static Theft t;
	static Domestic d;
	static Medical m;
	@BeforeClass
	public static void initializeTheft(){
		t = new Theft("theft near by my house");
		m = new Medical("accident");
		d = new Domestic("brothers fighting");
	}
	
	@Test
	public void testTheft(){
		assertEquals("Theft problem test","Theft Problem is : theft near by my house", t.toString());
	}
	@Test
	public void testDomestic(){
		assertEquals("Domestic problem test","Domestic problem is : brothers fighting", d.toString());
	}
	@Test
	public void testMedical(){
		assertEquals("Medical problem test","Medical problem is : accident", m.toString());
	}
}
