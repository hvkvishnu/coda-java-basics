package day3.morning;

public class ClassObjectDemo2 {

	public static void main(String[] args) {
		int value = 1000;
		System.out.println("value..."+value);
		PBV pbv = new PBV();
		pbv.acceptValue(value);
		
		//when you pass primitive data, value only passed
		System.out.println("After passing Value : "+value);
		
		PBR pbr = new PBR();
		//when you pass complex data type, object is passed not value
		InkPen pen = new InkPen();
		System.out.println("Ink status..."+pen.ink);
		pbr.acceptObject(pen);
		System.out.println("Ink status after passing...."+pen.ink);
	}
}

class PBV{
	public void acceptValue(int value){
		value=0;
	}
}

class PBR{
	public void acceptObject(InkPen pen){
		pen.ink = "half";
	}
}

class InkPen{
	String ink = "full";
}