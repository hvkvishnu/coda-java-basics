package day3.morning;

public class ConsDemo {

	public static void main(String[] args) {
		Human man = new Human();
		System.out.println(man);
		new Human("from main");
		new Human(5);
	}
}

class Human{
	public Human(){
		this("from non parameter constructor");
		System.out.println("no parameters......");
	}
	
	public Human(String s){
		System.out.println("with string parameter....."+s);
	}
	
	public Human(int i){
		System.out.println("with int parameter....."+i);
	}

	@Override
	public String toString() {
		return "Human [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	
}