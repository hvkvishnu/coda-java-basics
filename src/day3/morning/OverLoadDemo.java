package day3.morning;

public class OverLoadDemo {
	public static void main(String[] args) {
		Help911 usa911help = new Help911();
		usa911help.help(new Domestic("brothers fighting"));
		usa911help.help(new Theft("theft near by my house"));
	}
}

class Help911{
	public void help(Theft t){
		System.out.println("Theft problem method called....:"+t);
	}
	
	public void help(Medical m){
		System.out.println("Medical problem method called....:"+m);
	}
	
	public void help(Domestic d){
		System.out.println("Domestic problem method called....:"+d);
	}
}

class Theft{
	String msg;
	public Theft(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return "Theft Problem is : "+this.msg;
	}
}

class Medical{
	String msg;
	public Medical(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return "Medical problem is : "+this.msg;
	}
}

class Domestic{
	String msg;
	public Domestic(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return "Domestic problem is : "+this.msg;
	}
}