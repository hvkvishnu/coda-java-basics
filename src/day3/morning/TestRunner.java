package day3.morning;

import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(TestSuite.class);
		System.out.println("Failure count..."+result.getFailureCount());
		
		List <Failure> failures = result.getFailures();
		for(Failure f : failures){
			System.out.println(f.toString());
		}
		
		System.out.println("Runtime..."+result.getRunTime());
		System.out.println("Run count..."+result.getRunCount());
		System.out.println("Is successful..."+result.wasSuccessful());
	}
}
