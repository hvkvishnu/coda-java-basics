package day3.morning;

public class ClassObjectDemo3 {

	public static void main(String[] args) {
		ReservationCounter central = new ReservationCounter();
		Customer1 ramu = new Customer1();
		System.out.println("Money with ramu before booking..."+ramu.getMoney());
		central.bookTicket(new ReservationSlip(), ramu);
		System.out.println("Money with ramu after booking..."+ramu.getMoney());
		
		
	}

}

class ReservationCounter{
	public void bookTicket(ReservationSlip slip,Customer1 customer){
		System.out.println(slip+"ticket booking for this slip...."+customer.getMoney()+" is the money");
		customer.deductMoney(500);
	}
	
}

class Customer1{
	private int money = 1000;
	
	public int getMoney(){
		return this.money;
	}
	
	public void deductMoney(int amt){
		this.money -= amt;
	}
}

class ReservationSlip{
	
}