package shipment.exercise;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import shipment.exercise.Constraints.WorkHours;

public class ShipmentDemo {
	public static void main(String[] args) {
		Shipment shipment = new Shipment();
		shipment.execute(LocalDateTime.of(2020, 8, 15, 11, 0),Duration.ofHours(72));
		shipment.showStatus();
	}
}
class Shipment{
	Cargo cargo;
	LocalDateTime bookTime;
	LocalDateTime deliveredDate;
	public void execute(LocalDateTime bookTime,Duration d){
		this.bookTime = bookTime;
//		Cargo cargo = new Cargo(bookTime,Duration.ofHours(10));
		cargo = new Cargo(bookTime, d);
		
		cargo.initalCheck();
		
		while(!cargo.hourNeeded.isZero()){
			if(cargo.checkAllConstraints()){
				cargo.skipOneDay();
			}
			else{
				cargo.setRemainingHour();
			}
		}
	}
	
	public void showStatus(){
		ZoneId zoneId = ZoneId.of("Asia/Kolkata");
		long hours = cargo.calculateDeliver();
		System.out.println("Indian time zone for booked date:"+bookTime.atZone(zoneId));
		System.out.println("Booked at ...:"+bookTime.toLocalDate()+":"+bookTime.toLocalTime());
		System.out.println("Delivered in..:"+hours+" hrs");
//		deliveredDate = cargo.bookDateTime.plusHours(hours);
//		System.out.println("Current time..:"+cargo.currentDateTime);
		deliveredDate = cargo.currentDateTime;
		System.out.println("Delivered at..:"+deliveredDate.toLocalDate()+":"+deliveredDate.toLocalTime());
		System.out.println("Indian time Zone for deliver date..:"+deliveredDate.atZone(zoneId));
	}
}

