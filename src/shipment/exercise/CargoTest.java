package shipment.exercise;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
public class CargoTest {
	private static Cargo cargo;
	@BeforeClass
	public static void initailize(){
		cargo = new Cargo(LocalDateTime.of(2020, 8, 15, 22, 0), Duration.ofHours(72));
	
	}
	@Test
	public void testWeekend(){
		assertTrue(cargo.checkWeekend());
	}
	
	@Test
	public void testAug(){
		assertTrue(cargo.checkAUG15());
	}
	
	@Test
	public void testJan1(){
		assertFalse(cargo.checkJAN1());
	}
	
	@Test
	public void testJan25(){
		assertFalse(cargo.checkJAN25());
	}
	
	@Test
	public void testAll(){
		assertTrue(cargo.checkAllConstraints());
	}
	
	@Ignore
	@Test
	public void testBeforeOpen(){
		assertTrue(cargo.checkBeforeOpen(cargo.currentDateTime.toLocalTime()) < 0);
	}
	
	@Test
	public void testAfterOpen(){
		assertTrue(cargo.checkAfterClose(cargo.currentDateTime.toLocalTime()) > 0);
	}
}
