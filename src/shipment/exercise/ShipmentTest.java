package shipment.exercise;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.LocalDateTime;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
public class ShipmentTest {
	private static Shipment sp;
	@BeforeClass
	public static void initialize(){
		sp = new Shipment();
	}
	@Test
	public void testDeliveredDate(){
		sp.execute(LocalDateTime.of(2020, 9, 22, 22, 25),Duration.ofHours(26));
		sp.showStatus();
		assertEquals(LocalDateTime.of(2020, 9, 25, 9, 0), sp.deliveredDate);
	}
	
	@Ignore
	@Test
	public void testEndOfYear(){
		sp.execute(LocalDateTime.of(2019, 12, 31, 10, 0), Duration.ofHours(24));
		sp.showStatus();
		assertEquals(LocalDateTime.of(2020, 1, 3, 10, 0), sp.deliveredDate);
	}
	
	@Ignore
	@Test
	public void testLeapYear(){
		sp.execute(LocalDateTime.of(2020, 2, 28, 10, 0), Duration.ofHours(24));
		sp.showStatus();
		assertEquals(LocalDateTime.of(2020, 3, 3, 10, 0), sp.deliveredDate);
	}
	
	@Ignore
	@Test
	public void testBetweenAug15(){
		sp.execute(LocalDateTime.of(2020, 8, 15, 5, 0), Duration.ofHours(72));
		sp.showStatus();
		assertEquals(LocalDateTime.of(2020, 8, 25, 7, 0), sp.deliveredDate);
	}
}
