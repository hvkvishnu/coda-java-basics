package shipment.exercise;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CargoTest.class,ShipmentTest.class,ConstraintsTest.class})
public class TestSuite {

}
