package shipment.exercise;

import org.junit.Test;

import shipment.exercise.Constraints.LeaveConstraint;

import static org.junit.Assert.*;

import java.time.LocalDate;
public class ConstraintsTest {
	@Test
	public void checkSatSun(){
		LocalDate ld = LocalDate.of(2020, 9, 20);
		assertTrue(LeaveConstraint.SATSUN.check(ld));
		assertFalse(LeaveConstraint.SATSUN.check(ld.plusDays(2)));
	}
	@Test
	public void checkAug15(){
		LocalDate ld = LocalDate.of(2020, 8, 15);
		assertTrue(LeaveConstraint.AUG15.check(ld));
	}
	@Test
	public void checkJan1(){
		LocalDate ld = LocalDate.of(2020, 1, 1);
		assertTrue(LeaveConstraint.JAN1.check(ld));
	}
	@Test
	public void checkJan25(){
		LocalDate ld = LocalDate.of(2020, 1, 25);
		assertTrue(LeaveConstraint.JAN25.check(ld));
	}
}
