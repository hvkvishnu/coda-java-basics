package shipment.exercise;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

public class Constraints {
	enum LeaveConstraint{
		SATSUN{
			@Override
			boolean check(LocalDate d) {
				return d.getDayOfWeek() == DayOfWeek.SATURDAY || d.getDayOfWeek() == DayOfWeek.SUNDAY;
			}
		},
		AUG15{
			@Override
			boolean check(LocalDate d) {
				int day =d.getDayOfMonth();
				return day==15 && d.getMonth() == Month.AUGUST;
			}
		},
		JAN1{
			@Override
			boolean check(LocalDate d) {
				return d.getDayOfMonth() == 1 && d.getMonth() == Month.JANUARY;
			}
		},
		JAN25{
			@Override
			boolean check(LocalDate d) {
				return d.getDayOfMonth() == 25 && d.getMonth() == Month.JANUARY;
			}
		};
		abstract boolean check(LocalDate d);
	}
	
	enum WorkHours{
		OPEN(LocalTime.of(7, 0)),CLOSE(LocalTime.of(19, 0));
		private LocalTime time;
		private WorkHours(LocalTime lt) {
			this.time = lt;
		}
		
		public LocalTime getOpen(){ 
			return OPEN.time;
		}
		
		public LocalTime getClose(){ 
			return CLOSE.time;
		}
	}
}
