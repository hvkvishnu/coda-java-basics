package shipment.exercise;

import java.util.List;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;



public class TestRunner {
	public static void main(String[] args) {
		Result   rslt=	JUnitCore.runClasses(ConstraintsTest.class,ShipmentTest.class,CargoTest.class);
		
		System.out.println(rslt.getFailureCount());
		
		List <Failure> lis= rslt.getFailures();
		
		for( Failure i: lis)
		{
			System.out.println(i.toString());
		}
		
		System.out.println(rslt.getRunTime());
		
		System.out.println(rslt.wasSuccessful()); 
		
		System.out.println(rslt.getRunCount());
		
	}
}
