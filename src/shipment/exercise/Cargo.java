package shipment.exercise;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

import shipment.exercise.Constraints.LeaveConstraint;
import shipment.exercise.Constraints.WorkHours;

public class Cargo {
	LocalDateTime bookDateTime;
	LocalDateTime currentDateTime;
	Duration hourNeeded;
	Duration remainingHour;
	LeaveConstraint leaveConstraint;
	WorkHours workHours;
	
	
	public Cargo(LocalDateTime ldt,Duration hd) {
		this.bookDateTime = ldt;
		this.currentDateTime = ldt;
		this.hourNeeded = hd;
	}
	
	public boolean checkWeekend(){
		leaveConstraint = LeaveConstraint.valueOf("SATSUN");
		return leaveConstraint.check(currentDateTime.toLocalDate());
	}
	
	public boolean checkAUG15(){
		leaveConstraint = LeaveConstraint.valueOf("AUG15");
		return leaveConstraint.check(currentDateTime.toLocalDate());
	}
	public boolean checkJAN1(){
		leaveConstraint = LeaveConstraint.valueOf("JAN1");
		return leaveConstraint.check(currentDateTime.toLocalDate());
	}
	public boolean checkJAN25(){
		leaveConstraint = LeaveConstraint.valueOf("JAN25");
		return leaveConstraint.check(currentDateTime.toLocalDate());
	}
	
	public boolean checkAllConstraints(){
		return checkWeekend() || checkAUG15() || checkJAN1() || checkJAN25();
	}
	public void setRemainingHour(){
		Duration diff = Duration.between( currentDateTime.toLocalTime(),WorkHours.CLOSE.getClose());
		if(hourNeeded.compareTo(diff) >= 0){
			this.hourNeeded = hourNeeded.minus(diff);
			skipOneDay();
		}
		else{
			this.currentDateTime = this.currentDateTime.plus(this.hourNeeded);
			this.hourNeeded = Duration.ofHours(0);
		}
	}
	
	public void skipOneDay(){
		this.currentDateTime = this.currentDateTime.plusDays(1);
		this.currentDateTime = LocalDateTime.of(this.currentDateTime.toLocalDate(), WorkHours.OPEN.getOpen());
	}
	
	public long calculateDeliver(){
		return Duration.between(this.bookDateTime, this.currentDateTime).toHours();
	}
	
	public void initalCheck(){
		LocalTime lt = this.currentDateTime.toLocalTime();
		if(checkBeforeOpen(lt) < 0){
			this.currentDateTime = LocalDateTime.of(this.currentDateTime.toLocalDate(), WorkHours.OPEN.getOpen());
		}
		
		if(checkAfterClose(lt) > 0){
			skipOneDay();
		}
	}
	
	public long checkAfterClose(LocalTime lt) {
		return lt.compareTo(WorkHours.CLOSE.getClose());
	}

	public long checkBeforeOpen(LocalTime lt){
		return lt.compareTo(WorkHours.OPEN.getOpen());
	}
	
}
