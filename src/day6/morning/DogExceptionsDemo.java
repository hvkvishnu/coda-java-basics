  package day6.morning;

public class DogExceptionsDemo {

	public static void main(String[] args) {
		Dog tiger = new Dog();
		Child child = new Child();
		Item item = new Stick();
		child.playWithDog(tiger, item);
	}

}
class Dog{
	public void play(Item item) throws DogExceptions{
		item.execute();
	}
}
class Child{
	public void playWithDog(Dog dog,Item item) {
		try{
			dog.play(item);
		}catch(DogExceptions de){
			de.visit();
		}
	}
}
class Handler911{
	
	public void handle(DogBiteException dbe){
		System.out.println("Take him hospital...."+dbe);
	}
	public void handle(DogBarkException dre){
		System.out.println("dont worry...."+dre);
	}
	public void handle(DogHappyException dhe){
		System.out.println("gimme some more...."+dhe);
	}
}
abstract class Item{
	public abstract void execute() throws DogExceptions;
}
class Stone extends Item {
	@Override
	public void execute() throws DogExceptions{
		throw new DogBarkException();
	}
}
class Stick extends Item{
	@Override
	public void execute() throws DogExceptions {
		throw new DogBiteException();
	}
}
class Biscuit extends Item{
	@Override
	public void execute() throws DogExceptions {
		throw new DogHappyException();
	}
}
abstract class DogExceptions extends Exception{
	private static Handler911 h911;
	static{
		h911 = new Handler911();
	}
	public Handler911 getHandler(){
		return h911;
	}
	public abstract void visit();
}
class DogBiteException extends DogExceptions{
	@Override
	public void visit() {
		getHandler().handle(this);
	}
}
class DogBarkException extends DogExceptions{
	@Override
	public void visit() {
		getHandler().handle(this);
	}
}
class DogHappyException extends DogExceptions{
	@Override
	public void visit() {
		getHandler().handle(this);
	}
}
