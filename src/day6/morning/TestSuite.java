package day6.morning;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CustomExceptionTest.class,DogExceptionTest.class})
public class TestSuite {

}
