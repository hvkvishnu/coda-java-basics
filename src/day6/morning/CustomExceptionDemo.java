package day6.morning;

import java.util.Scanner;

public class CustomExceptionDemo {

	public static void main(String[] args) throws Exception {
		TestCustomException te = new TestCustomException();
		try{			
			te.test("ramu");
		}catch (Throwable t) {
			System.out.println(t);
		}
	}

}
class TestCustomException{
	public void test(String s) throws Exception{	
//		Scanner sc = new Scanner(System.in);
		if(s.equals("ramu"))
			throw new MyException();
	}	
}
class MyException extends Exception{
	public MyException() {
		System.out.println("Exception caught.....");
	}
	@Override
	public String toString() {
		return "ramu arised";
	}
}