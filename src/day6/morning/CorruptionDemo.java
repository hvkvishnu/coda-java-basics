package day6.morning;

public class CorruptionDemo {

	public static void main(String[] args) {
		PoliceService ps = new PoliceService();
		HospitalService hs = new HospitalService();
		
		EsevaDemo ed = new EsevaDemo();
		ed.setCommand(1, ps);
		ed.execute(1);
		ed.setCommand(2, hs);
		ed.execute(2);
	}
}
class Corporation{
	public void deathCertificate(){
		System.out.println("death certificate is issued....");
	}
}
class Police{
	public void doInvestigation(){
		System.out.println("investigation going on...");
	}
}
class Hospital{
	public void doPostMortem(){
		System.out.println("do postmortem....");
	}
}