package day6.morning;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DogExceptionTest {
	 static Dog ceaser;
	@BeforeClass
	public static void initializeDog(){
		ceaser = new Dog();
	}
	@Test(expected = DogBarkException.class)
	public void testDogbark() throws DogExceptions{
		Stone stone = new Stone();
//		baby.playWithDog(ceaser, stone);
		ceaser.play(stone);
	}
	@Test(expected = DogBiteException.class)
	public void testDogBite() throws DogExceptions{
		Stick stick = new Stick();
		ceaser.play(stick);
	}
	@Test(expected = DogHappyException.class)
	public void testDogHappy() throws DogExceptions{
		Biscuit goodDay = new Biscuit();
		ceaser.play(goodDay);
	}
	
	
}
