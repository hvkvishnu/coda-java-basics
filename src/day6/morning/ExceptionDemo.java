package day6.morning;

public class ExceptionDemo {

	public static void main(String[] args) {
		System.out.println("before exception..");
		try{
			int i = 1/0;
		}catch(ArithmeticException ae){
			System.out.println(ae);
		}
		catch(ArrayIndexOutOfBoundsException ai){
			System.out.println(ai);
		}
		finally{
			System.out.println("Finally called.....");
		}
		System.out.println("after exception called.....");
	}
}
