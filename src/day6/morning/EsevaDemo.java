package day6.morning;

public class EsevaDemo {
	EsevaCommand ec[] = new EsevaCommand[5];
	public EsevaDemo() {	
		for(int i=0;i<5;i++){
			ec[i] = new DummyCommand();
		}
	}
	public void setCommand(int slot,EsevaCommand esevaCommand){
		ec[slot]=esevaCommand;
	}
	public void execute(int slot){
		ec[slot].execute();
	}
	
}
abstract class EsevaCommand{
	Police p;
	Hospital hp;
	Corporation cp;
	public EsevaCommand(){
		init();
	}
	public void init(){
		p = new Police();
		hp= new Hospital();
		cp = new Corporation();
	}
	public abstract void execute();
}
class DummyCommand extends EsevaCommand{
	@Override
	public void execute() {	
	}
}
class PoliceService extends EsevaCommand{
	@Override
	public void execute() {
		p.doInvestigation();
	}
}
class HospitalService extends EsevaCommand{
	@Override
	public void execute() {
		hp.doPostMortem();
	}
}
class CorporatonService extends EsevaCommand{
	@Override
	public void execute() {
		cp.deathCertificate();
	}
}